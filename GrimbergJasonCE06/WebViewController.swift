//
//  WebViewController.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 10/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate {

    var webView : WKWebView!
    var urlString: String = ""
    var webTitle: String = ""
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
        
        self.title = webTitle
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Check to see if the URL is empty
        if urlString != "" {
            let myURL = URL(string: urlString)
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        } else {
            let myURL = URL(string: "https://google.com")
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        performSegue(withIdentifier: "unwindToArticleView", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
