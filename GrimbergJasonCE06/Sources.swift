//
//  Sources.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 10/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation

//Model Object Representing a Reddit Post
class Sources {
    
    /* Stored Properites */
    var name: String
    var url: String
    var description: String?
    var id: String
    
    /* Computed Properties */
    
    /* Initializers */
    init(name: String, url: String, description: String, id: String){
        self.name = name
        self.url = url
        self.description = description
        self.id = id
    }
}
