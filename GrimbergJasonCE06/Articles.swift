//
//  Articles.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 10/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation
import UIKit

//Model Object Representing a Reddit Post
class Articles {
    
    /* Stored Properites */
    var author: String
    var title: String
    var url: String
    var urlToImage: String
    var description: String?
    var publishedAt: String
    var thumbNail: UIImage!
    
    /* Computed Properties */
    
    /* Initializers */
    init(author: String, title: String, url: String, urlToImage: String, description: String?, publishedAt: String) {
        self.author = author
        self.title = title
        self.url = url
        self.urlToImage = urlToImage
        self.description = description
        self.publishedAt = publishedAt
        
        // Check to make sure if the thumbnail can return and download
        if let url = URL(string: urlToImage) {
            
            do {
                let data = try Data(contentsOf: url)
                self.thumbNail = UIImage(data: data)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
