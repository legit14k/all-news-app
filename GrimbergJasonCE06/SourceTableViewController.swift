//
//  SourceTableViewController.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 10/5/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class SourceTableViewController: UITableViewController {
    
    var currentPost = [Sources]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Change the title of the sources page
        self.title = "Sources"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return currentPost.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ID_Sources", for: indexPath) as? TableViewCell
            else { return tableView.dequeueReusableCell(withIdentifier: "ID_Sources", for: indexPath) }
        
        // Configure the cell
        cell.titleLabel?.text = currentPost[indexPath.row].name
        
        return cell
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            let postToSend = currentPost[indexPath.row]
            
            if let destination = segue.destination as? ArticleTableViewController {
                // Send over what source we pressed
                destination.articleID = postToSend.id
            }
        }
    }
}
