//
//  LoadingViewController.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 10/3/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    // Data Array for our initial download
    var loadSources = [Sources]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Download and parse data
        sourceDownloadJSON(atURL: "https://newsapi.org/v1/sources?language=en")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loadingSegue"
        {
            let destinationNavigationController = segue.destination as! UINavigationController
            let targetController = destinationNavigationController.topViewController as! SourceTableViewController
            // Add my current data to the tableView data
            for i in loadSources {
                targetController.currentPost.append(i)
            }
        }
    }
    
    func sourceDownloadJSON(atURL urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        if let validURL = URL(string: urlString) {
            
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { assertionFailure(); return
                }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        // Starting with the results array of objects and moving down
                        guard let outerData = json["sources"] as? [[String: Any]]
                            else { return }
                        
                        for inner in outerData {
                            guard let id = inner["id"] as? String,
                                let name = inner["name"] as? String,
                                let description = inner["description"] as? String,
                                let url = inner["url"] as? String
                                else { return }
                            
                            let currentPost = (Sources(name: name, url: url, description: description, id: id))
                            
                            self.loadSources.append(currentPost)
                        }
                    }
                }
                catch {
                    // Catch and print the error that we get
                    print(error.localizedDescription)
                }
                
                //Do UI Stuff
                DispatchQueue.main.async {
                    // Perform the segue from the loading view to the navigation controller
                    self.performSegue(withIdentifier: "loadingSegue", sender: nil)
                }
            })
            task.resume()
        }
    }
}

