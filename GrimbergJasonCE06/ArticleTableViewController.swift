//
//  ArticleTableViewController.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 10/6/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class ArticleTableViewController: UITableViewController {

    var tempString = ""
    var allArticles = [Articles]()
    
    let mainURL = "https://newsapi.org/v1/articles?source="
    var articleID = ""
    let articleAPI = "&apiKey=13a8fb3ba12941a38c9e6b7efa963031"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        articleDownloadJSON(atURL: mainURL + articleID + articleAPI)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allArticles.count
    }

    func articleDownloadJSON(atURL urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        if let validURL = URL(string: urlString) {
            
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        // Starting with the results array of objects and moving down
                        guard let outerData = json["articles"] as? [[String: Any]]
                            else { return }
                        
                        for inner in outerData {
                            
                            var newAuthor = ""
                            
                            var newTitle = ""
                            var newDesc = ""
                            var newUrl = ""
                            var newUrlToImage = ""
                            var newPublished = ""
                            // Check author
                            if let author = inner["author"] as? String {
                                newAuthor = author
                            } else {
                                newAuthor = "No Author"
                            }
                            // Check title
                            if let title = inner["title"] as? String {
                                newTitle = title
                            } else {
                                newTitle = "No Title"
                            }
                            // Check description
                            if let description = inner["description"] as? String {
                                newDesc = description
                            } else {
                                newDesc = "No description"
                            }
                            // Check url
                            if let url = inner["url"] as? String {
                                newUrl = url
                            } else {
                                newUrl = "No URL"
                            }
                            // Check the image
                            if let urlToImage = inner["urlToImage"] as? String {
                                if urlToImage.contains("https") {
                                    newUrlToImage = urlToImage
                                } else {
                                    newUrlToImage = ""
                                }
                            } else {
                                newUrlToImage = ""
                            }
                            
                            // Check the publish date
                            if let published = inner["publishedAt"] as? String {
                                newPublished = published
                            } else {
                                newPublished = "No publish"
                            }
                            
                            // Append all of the data to our custom model
                            let currentPost = (Articles(author: newAuthor, title: newTitle, url: newUrl.description, urlToImage: newUrlToImage.description, description: newDesc, publishedAt: newPublished))
                            
                            self.allArticles.append(currentPost)
                        }
                    }
                }
                catch {
                    // Catch and print the error that we get
                    print(error.localizedDescription)
                }
                
                //Do UI Stuff
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
            task.resume()
        }
    }
    
    // MARK: - Header Methods
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Set the title for the header
        return "All Articles"
    }
    
    // Set header height
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Reuse an existing cell or create a new one if needed
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ID_Articles", for: indexPath) as? ArticleTableViewCell
            else { return tableView.dequeueReusableCell(withIdentifier: "ID_Articles", for: indexPath) }
        
        // Configure cell
        cell.titleLabel?.text = allArticles[indexPath.row].title.description
        
        // Check to see if there is a thumbnail
        if allArticles[indexPath.row].urlToImage != "" {
            cell.articleImage?.image = allArticles[indexPath.row].thumbNail
        } else {
            cell.articleImage?.image = #imageLiteral(resourceName: "defaultImage")
        }
        
        // Return our configured cell
        return cell
    }
    
    // Unwind to the articles screen
    @IBAction func unwindToRoot(segue: UIStoryboardSegue) {}
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            let postToSend = allArticles[indexPath.row]
            
            if let destination = segue.destination as? DetailsViewController {
                // Send over what source we pressed
                destination.post = postToSend
            }
        }
    }
}
