//
//  DetailsViewController.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 9/10/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var urlImage: UIImageView!
    
    var post: Articles!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Change the title of the page to the name
        self.title = "Details"
        
        if post != nil {
            // Set the header name
            nameLabel?.text = post.title
            
            // Check if there is a description can't have this blank
            if post.description != "" {
                descriptionLabel?.text = post.description
            } else {
                descriptionLabel?.text = "No description found"
            }
            
            // Check and set the image to default if there is no thumbnail
            if post.thumbNail != nil {
                // Add the image when the user chooses a link
                downloadPicture(imageURL: post.urlToImage)
            } else {
                urlImage.image = #imageLiteral(resourceName: "defaultImage")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Download and the parse the data for image
    func downloadPicture(imageURL: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        if let validURL = URL(string: imageURL) {
            let request = URLRequest(url: validURL)
            
            let task = session.dataTask(with: request, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return
                }
                
                let image = UIImage(data: data)
                
                //Do UI Stuff
                DispatchQueue.main.async {
                    self.urlImage.image = image
                }
            })
            // Resume the task
            task.resume()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? WebViewController {
            destination.urlString = post.url
            destination.webTitle = post.title
        }
    }
}
