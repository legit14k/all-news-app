//
//  TableViewCell.swift
//  GrimbergJasonCE06
//
//  Created by Jason Grimberg on 9/10/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
